//
//  ViewController.swift
//  SceneKitTest1
//
//  Created by admin on 29.11.17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import SceneKit
class ViewController: UIViewController {


    @IBOutlet var sceneView: SCNView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewDidAppear(_ animated: Bool) {
        sceneSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    

    @IBAction func startStopCamera(_ sender: UIButton) {
        if (sceneView.allowsCameraControl) {
            sceneView.allowsCameraControl = false
        } else {
           sceneView.allowsCameraControl = true
        }
    }
    func sceneSetup() {
        let scene = SCNScene()
        // 2
        let sphereGeometry = SCNSphere(radius: 5.0)
        //let boxGeometry = SCNBox(width: 10.0, height: 10.0, length: 10.0, chamferRadius: 0.0)
        let sphereNode = SCNNode(geometry: sphereGeometry)
        scene.rootNode.addChildNode(sphereNode)
        //
        sceneView.scene = scene
        sceneView.autoenablesDefaultLighting = true
        sceneView.allowsCameraControl = true
        sceneView.showsStatistics = true
        sceneView.debugOptions = .showWireframe
        let vertexSources = sphereGeometry.sources(for: .vertex)[0]
        sphereGeometry.elements
        var stride:Int = vertexSources.dataStride
        var offset:Int = vertexSources.dataOffset
        var componentsPerVector = vertexSources.componentsPerVector;
        var bytesPerVector = componentsPerVector * vertexSources.bytesPerComponent;
        var vectorCount = vertexSources.vectorCount;
        var vertices:[SCNVector3] // A new array for vertices
        // for each vector, read the bytes
        for i in 0..<vectorCount {
            
            // Assuming that bytes per component is 4 (a float)
            // If it was 8 then it would be a double (aka CGFloat)
            var vectorData:Array<Any>
            var ptr: UnsafeMutablePointer<UInt8>
            // The range of bytes for this vector
            var byteRange: Range<Int> = i*stride + offset..<bytesPerVector
            //var byteRange:NSRange = NSMakeRange(i*stride + offset, bytesPerVector);   // and read the lenght of one vector
            // Read into the vector data buffer
            vertexSources.data.copyBytes(to: ptr, from:byteRange)
            //[vertexSource.data getBytes:&vectorData range:byteRange];
//
//            // At this point you can read the data from the float array
//            float x = vectorData[0];
//            float y = vectorData[1];
//            float z = vectorData[2];
//
//            // ... Maybe even save it as an SCNVector3 for later use ...
//            vertices[i] = SCNVector3Make(x, y, z);
        }
    }

}

